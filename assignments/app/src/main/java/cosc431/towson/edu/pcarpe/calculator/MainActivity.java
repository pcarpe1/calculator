package cosc431.towson.edu.pcarpe.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.DecimalFormat;

import cosc431.towson.edu.assignments.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button one, two, three, four, five, six, seven,
    eight, nine, zero, point, clear, plus, minus,
    times, divide, equals;
    TextView display, ghostDisplay;
    double val1, val2;
    boolean addIt, subIt, divIt, multIt;
    DecimalFormat df = new DecimalFormat("##.#");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setWidgets();
        setToFalse();

    }

    boolean checkOperators() {
        if (display.getText().toString().contains("+") ||
                display.getText().toString().contains("-") ||
                display.getText().toString().contains("×") ||
                display.getText().toString().contains("÷")) {
            return true;
        }
        return false;
    }

    boolean checkEnd(String value) {
        if (display.getText().toString().endsWith(value)) {return true;}
        return false;
    }

    void setToFalse() {
        multIt = false;
        addIt = false;
        subIt = false;
        divIt = false;
    }

    void setWidgets() {
        //buttons
        one = (Button) findViewById(R.id.button1);
        two = (Button) findViewById(R.id.button2);
        three = (Button) findViewById(R.id.button3);
        four = (Button) findViewById(R.id.button4);
        five = (Button) findViewById(R.id.button5);
        six = (Button) findViewById(R.id.button6);
        seven = (Button) findViewById(R.id.button7);
        eight = (Button) findViewById(R.id.button8);
        nine = (Button) findViewById(R.id.button9);
        zero = (Button) findViewById(R.id.button0);
        point = (Button) findViewById(R.id.buttonPoint);
        clear = (Button) findViewById(R.id.buttonClear);
        plus = (Button) findViewById(R.id.plus);
        minus = (Button) findViewById(R.id.minus);
        times = (Button) findViewById(R.id.multiply);
        divide = (Button) findViewById(R.id.divide);
        equals = (Button) findViewById(R.id.equals);
        //textview
        display = (TextView) findViewById(R.id.display);
        ghostDisplay = (TextView) findViewById(R.id.ghostDisplay);
        //OnClickListener
        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        zero.setOnClickListener(this);
        point.setOnClickListener(this);
        clear.setOnClickListener(this);
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        times.setOnClickListener(this);
        divide.setOnClickListener(this);
        equals.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.button1:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("1");
                break;
            case R.id.button2:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("2");
                break;
            case R.id.button3:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("3");
                break;
            case R.id.button4:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("4");
                break;
            case R.id.button5:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("5");
                break;
            case R.id.button6:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("6");
                break;
            case R.id.button7:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("7");
                break;
            case R.id.button8:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("8");
                break;
            case R.id.button9:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("9");
                break;
            case R.id.button0:
                if (checkOperators()) {
                    ghostDisplay.setText(display.getText());
                    display.setText("");
                }
                display.append("0");
                break;
            case R.id.buttonPoint:
                if(checkEnd(".") ||
                        checkOperators() ||
                        display.getText().toString().contains(".")) {break;}
                display.append(".");
                break;
            case R.id.buttonClear:
                display.setText("");
                val1 = 0;
                setToFalse();

                break;
            case R.id.plus:
                if(!checkOperators()) {
                    val1 = Double.parseDouble(display.getText().toString());
                    display.append(" + ");
                } else {
                    String temp = display.getText().toString();
                    display.setText(temp.replace(temp.charAt(temp.length() - 2), '+'));
                }
                setToFalse();
                addIt=true;
                break;
            case R.id.minus:
                if(!checkOperators()) {
                    val1 = Double.parseDouble(display.getText().toString());
                    display.append(" - ");
                } else {
                    String temp = display.getText().toString();
                    display.setText(temp.replace(temp.charAt(temp.length() - 2), '-'));
                }
                setToFalse();
                subIt = true;
                break;
            case R.id.multiply:
                if(!checkOperators()) {
                    val1 = Double.parseDouble(display.getText().toString());
                    display.append(" × ");
                } else {
                    String temp = display.getText().toString();
                    display.setText(temp.replace(temp.charAt(temp.length() - 2), '×'));
                }
                setToFalse();
                multIt = true;
                break;
            case R.id.divide:
                if(!checkOperators()) {
                    val1 = Double.parseDouble(display.getText().toString());
                    display.append(" ÷ ");
                } else {
                    String temp = display.getText().toString();
                    display.setText(temp.replace(temp.charAt(temp.length() - 2), '÷'));
                }
                setToFalse();
                divIt = true;
                break;
            case R.id.equals:
                ghostDisplay.setText("");
                val2 = Double.parseDouble(display.getText().toString());
                double answer = 0;
                if(addIt) {answer = val1 + val2;}
                else if (subIt) {answer = val1 - val2;}
                else if (multIt) {answer = (val1*val2);}
                else if (divIt) {
                    if(val2 == 0)
                    {
                        break;
                    }
                    answer = (val1/val2);
                }
                else{
                    Toast.makeText(getParent(), "Error, restart app", Toast.LENGTH_LONG);
                }
                display.setText(String.valueOf(df.format(answer)));
                val1 = answer;
                setToFalse();

                break;
        }
    }
}
