package com.example.pcarpe1.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements ButtonFragment.OnButtonClick {
    TextView ratingTv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ratingTv = findViewById(R.id.ratingTv);

        ButtonFragment fragment;

        FragmentManager fgMngr = getSupportFragmentManager();

        fragment = (ButtonFragment)fgMngr.findFragmentByTag("BUTTON_FRAGMENT");
    }

    @Override
    public void buttonWasClicked(String msg) {
        ratingTv.setText(msg);

    }
}
